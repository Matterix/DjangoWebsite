
from django.contrib import admin
from django.urls import path,include

#Table of contents for your website.
#Something.com/urlpatterns


urlpatterns = [
    path('admin/', admin.site.urls),
    path('music/', include('music.urls')),
    path('projects/', include('projects.urls')),
]
