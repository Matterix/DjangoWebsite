from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Album,Song


'''
Benytter generic views. Generic views er oprettet fordi udviklere altid bruger de samme generic functions (se en liste af items, og details på specifikke items)
Derfor er der blevet lavet generic views i Django som gør det nemmere at arbejde med de functions.
'''


class IndexView(generic.ListView):
    template_name = 'music/index.html'

    def get_queryset(self):
        return Album.objects.all()

    '''
        Når man skal bruge listen ude i view skal man kalde objects_list
        Hvis man vil ændre navnet på denne variabel kan man override følgende
        context_object_name = 'album_list'
    '''


class DetailView(generic.DetailView):
    model = Album
    template_name = 'music/detail.html'


class AlbumCreate(CreateView):
    model = Album
    fields = ['artist', 'album_title', 'genre', 'album_logo']


''' Måden at gøre det på som ikke er Django generic.

from django.shortcuts import render, get_object_or_404
from .models import Album, Song


def index(request):
    all_albums = Album.objects.all()
    return render(request, 'music/index.html', {'all_albums' : all_albums})

def detail(request, album_id):
    #Try to get object, if not found it will raise a 404
    album = get_object_or_404(Album, id=album_id)
    return render(request, 'music/detail.html', {'album' : album})
'''